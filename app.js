const express = require('express');
const app = express();

const quoteRoutes = require('./routes');


app.use(express.json());

app.use('/api', quoteRoutes);

app.use((req, res, next) => {
    const err = new Error("Not Found");
    next(err);
});

app.use((err, req, res, next) => {
    
    res.status(err.status || 500);
    console.log(err);

    res.json({
        error: err.message
    });
});


app.listen(3000, () => console.log('Quote API listening on port 3000!'));
