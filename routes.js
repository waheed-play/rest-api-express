const express = require('express');

const router = express.Router();

const records = require('./records');

function asyncHandler(cb) {
    return async (req, res, next) => {
        try {
            await cb(req, res, next)
        } catch (error) {
            next(error);
        }
    }
}



router.get('/quotes', async (req, res) => {
    
    try {
        const quotes = await records.getQuotes();
        res.json({quotes});
    } catch (error) {
        res.json({error: error.message});
    }
});

router.get('/quotes/quote/random', async (req, res) => {
    
    try {
        const quote = await records.getRandomQuote();
        
        res.json(quote);
    } catch (error) {
        res.status(500).json({error: error.message});
    }
});

router.get('/quotes', async (req, res) => {
    
    try {
        const quotes = await records.getQuotes();
        res.json({quotes});
    } catch (error) {
        res.json({error: error.message});
    }
});

router.post('/quotes',  asyncHandler( async(req, res) => {
 
    if (!req.body.author && !req.body.quote) {
        res.status(400).json({error: 'Quote and Author is required'});
    }

    const quote = await records.createQuote({
        quote: res.body.quote,
        author: req.body.author
    });
    res.status(201).json(quote);
    
}));

router.put('/quotes/:id', async (req, res) => {
    try {

        if (!req.body.author && !req.body.quote) {
          return  res.status(400).json({error: 'Quote and Author is required'});
        }
        let quote = records.getQuote(req.params.id);
        if (!quote) {
           return res.status(400).json({error: 'Quote not found!'});
        }

         quote = records.createQuote({
            quote: res.body.quote,
            author: req.body.author,
            id: quote.id
        });
        res.status(204).end();
    } catch (error) {
        res.status(500).json({error: error.message});
    }
});

router.delete('/quotes/:id', async (req, res) => {
    try {
        const quote = records.getQuote(req.params.id);
        if (!quote) {
            return res.status(400).json({error: 'Quote not found!'});
        }

        await records.deleteQuote(quote);
        res.status(204).end();
    } catch (error) {
        res.status(500).json({error: error.message});
    }
});

module.exports = router;